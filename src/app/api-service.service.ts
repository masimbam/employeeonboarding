import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Convictions } from './convictions';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(private http:HttpClient) { 


  }

  public getToken(loginPayload){
const headers={

  'Authorization':'Basic'+btoa('mobile:123456'),
  'Content-type':'application/x-www-form-urlencoded'
}

return this.http.post('http://localhost:8080/'+'oauth/token',loginPayload,{headers})

  }

  public getConvictions(criteteria:String,value:String):Observable<Convictions[]>{

    return this.http.get<Convictions[]>('http://localhost:9072/convictions?search='+criteteria+'=='+value);
  }
}
