import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxComponentComponent } from './tax-component.component';

describe('TaxComponentComponent', () => {
  let component: TaxComponentComponent;
  let fixture: ComponentFixture<TaxComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
