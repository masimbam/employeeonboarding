import { Component, OnInit } from '@angular/core';
import { InteractionService } from '../interaction.service';
import { FormGroup } from '@angular/forms';
import { Employee } from '../employee';

@Component({
  selector: 'app-tax-component',
  templateUrl: './tax-component.component.html',
  styleUrls: ['./tax-component.component.css']
})
export class TaxComponentComponent implements OnInit {
message1:FormGroup;
  tax:number;
  constructor(private _interactionService:InteractionService) { 


  }

  ngOnInit() {

    this._interactionService.employeeMessage$.subscribe
    (
message => {
this.message1=message;
console.log("CALCULATING TAX"+this.message1.get('income').value+"National ID is..."+this.message1.get('nationalId').value);

this.tax=this.message1.get('income').value*0.1;

console.log("tax is...."+this.tax);

console.log("HIT THE UPDATE TAX API")

} 

    );
  }

}
