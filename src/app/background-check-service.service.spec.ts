import { TestBed } from '@angular/core/testing';

import { BackgroundCheckServiceService } from './background-check-service.service';

describe('BackgroundCheckServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackgroundCheckServiceService = TestBed.get(BackgroundCheckServiceService);
    expect(service).toBeTruthy();
  });
});
