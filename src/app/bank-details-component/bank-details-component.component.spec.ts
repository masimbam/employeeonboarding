import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankDetailsComponentComponent } from './bank-details-component.component';

describe('BankDetailsComponentComponent', () => {
  let component: BankDetailsComponentComponent;
  let fixture: ComponentFixture<BankDetailsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankDetailsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankDetailsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
