import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { InteractionService } from '../interaction.service';

@Component({
  selector: 'app-bank-details-component',
  templateUrl: './bank-details-component.component.html',
  styleUrls: ['./bank-details-component.component.css']
})
export class BankDetailsComponentComponent implements OnInit {

  message1:FormGroup;
 
  constructor(private _interactionService:InteractionService) { 


  }

  ngOnInit() {

    this._interactionService.employeeMessage$.subscribe
    (
message => {
this.message1=message;
console.log("Open a bank account...."+this.message1.get('income').value+"...National ID is..."+this.message1.get('nationalId').value);

console.log("HIT THE UPDATE BANK DETAILS API")

} 

    );

  }
}
