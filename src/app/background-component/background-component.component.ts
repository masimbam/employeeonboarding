import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { InteractionService } from '../interaction.service';
import { FormGroup } from '@angular/forms';
import { BackgroundCheckServiceService } from '../background-check-service.service';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-background-component',
  templateUrl: './background-component.component.html',
  styleUrls: ['./background-component.component.css']
})
export class BackgroundComponentComponent implements OnInit {
  message1:FormGroup;

  convictions=[];

 // url="http://localhost:9072/convictions?search=conviction==r*";
  constructor( private apiService:ApiServiceService,private _backgroundService:BackgroundCheckServiceService,private _interactionService:InteractionService) { }

  ngOnInit() {

    this._backgroundService.employeeMessage$.subscribe
    (
message => {
this.message1=message;
console.log("BACKGROUND CHECK...."+this.message1.get('income').value+"...National ID is..."+this.message1.get('nationalId').value);
}


 
    );

    
    const body=new HttpParams()
    .set('username','m@gmail.com')
    .set('password','123456')
    .set('grant_type','password')
    this.apiService.getToken(body.toString).subscribe(
data=>{

  window.sessionStorage.setItem('token',JSON.stringify(data))
}

    );


    this.apiService.getConvictions("nationalId",this.message1.get('nationalId').value).subscribe(
data=>this.convictions=data

    );


    if(this.convictions.length==0){

this._interactionService.sendMessage(this.message1);
    }else{

      console.log("REJECT EMPLOYEE")
    }

} 

    
}
  


 

