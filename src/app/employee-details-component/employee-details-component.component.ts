import { Component, OnInit } from '@angular/core';
import { InteractionService } from '../interaction.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ApiServiceService } from '../api-service.service';
import { HttpParams } from '@angular/common/http';
import { BackgroundCheckServiceService } from '../background-check-service.service';

@Component({
  selector: 'app-employee-details-component',
  templateUrl: './employee-details-component.component.html',
  styleUrls: ['./employee-details-component.component.css']
})
export class EmployeeDetailsComponentComponent implements OnInit {
  employeeForm=new FormGroup(

    {
      firstName:new FormControl(''),
      lastName:new FormControl(''),
      password:new FormControl(''),
      confirmPassword:new FormControl(''),
      gender:new FormControl(''),
      email:new FormControl(''),
      nationalId:new FormControl(''),
      income: new FormControl('')

    }
  );
  constructor(private _interactionService:InteractionService,private fb:FormBuilder,private  apiService:ApiServiceService,private  backgroundCheckServiceService:BackgroundCheckServiceService ) { }


  ngOnInit() {
   
  }

  registerEmployee(){

    
    console.log("DETAILS RECEIVED...."+this.employeeForm.get('firstName').value);

    const body=new HttpParams()
    .set('username','m@gmail.com')
    .set('password','123456')
    .set('grant_type','password')
    this.apiService.getToken(body.toString).subscribe(
data=>{

  window.sessionStorage.setItem('token',JSON.stringify(data))
}

    );

    this.backgroundCheckServiceService.sendMessage(this.employeeForm);

this.employeeForm.patchValue({

  firstName:'',
  lastName:'',
  password:'',
  confirmPassword:'',
  gender:'',
  email:'',
  nationalId:'',
  income: ''
})
  }

}
