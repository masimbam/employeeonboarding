import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaxComponentComponent } from './tax-component/tax-component.component';
import { BankDetailsComponentComponent } from './bank-details-component/bank-details-component.component';
import { EmployeeDetailsComponentComponent } from './employee-details-component/employee-details-component.component';
import {  ReactiveFormsModule} from '@angular/forms'
import { InteractionService } from './interaction.service';
import { HttpClientModule } from '@angular/common/http';
import { BackgroundComponentComponent } from './background-component/background-component.component';
@NgModule({
  declarations: [
    AppComponent,
    TaxComponentComponent,
    BankDetailsComponentComponent,
    EmployeeDetailsComponentComponent,
    BackgroundComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
