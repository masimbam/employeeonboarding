import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Employee } from './employee';
import {FormGroup} from '@angular/forms'
@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  private _employeeMessageSource=new Subject<FormGroup>();
employeeMessage$=this._employeeMessageSource.asObservable();
  constructor() { }

  sendMessage(message:FormGroup){

this._employeeMessageSource.next(message);
  }
}
